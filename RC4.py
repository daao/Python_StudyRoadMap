
# 白话解释 对称加密算法 VS 非对称加密算法
# https://segmentfault.com/a/1190000004461428

# RC4的加解密原理
# http://blog.csdn.net/wangqing008/article/details/39478463


# https://stackoverflow.com/questions/17549775/simple-encryption-algorithm-in-c-net/17549863#17549863
# https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
# http://www.cnblogs.com/sunxuchu/p/5483956.html

# 考虑RC4的对合性 

# data[i]^=key[i] 输入明文就是加密 输入加密就是明文

# RC4 Usage



data = "1111111111111111122222222222222222223333333333333333334"  
x = 0  
box = box = range(256)  
for i in xrange(256):  
    x = (x + box[i] + ord(data[i % len(data)])) % 256  
    box[i], box[x] = box[x], box[i]  
  
print box