#! /usr/bin/env python
#coding:utf8

# TextuePacker 根据目录批量导出 tps 然后根据 tps 批量导出 u3d 图集和tpsheet

# https://docs.python.org/2.7/library/xml.etree.elementtree.html#module-xml.etree.ElementTree

import sys
import os
import xml.etree.ElementTree as ET
# import chardet
import subprocess
import json

reload(sys)
sys.setdefaultencoding('utf-8')





# 图片目录
# ./Solider/1/Blue + /charge
# ./Solider/1/Blue + /dead


#
#

#
# # 模板路径



# 注意这个模块 windows 下编码不支持
# Our ISO-8859-2 and windows-1250 (Hungarian) probers have been temporarily disabled until we can retrain the models.
# Requires Python 2.6 or later
# https://github.com/chardet/chardet

def mconvert_to_utf8(sourceFileName, targetFileName):
    print sourceFileName + " => " + targetFileName
    with open(sourceFileName, "r") as sourceFile:
        source_content = sourceFile.read()
        with open(targetFileName,'w') as targetFile:
            targetFile.write(source_content.decode('gbk').encode('utf8'))

def exportTps(tpsheetExportPath, really_tpsheetExportPath, pngExportPath, prepath,export):
    tree = ET.parse(templatePath)
    root = tree.getroot()
    struct = root.find('struct')
    for	index in range(len(struct)):
        #<key>textureFileName</key>
		#<filename>G:/proJect/FightDay/Porject/Assets/Tmp/vip/test.tga</filename>
        if(struct[index].text == 'textureFileName'):
            # print index+1,struct[index+1].tag,struct[index+1].text
            struct[index+1].text = pngExportPath
        # <key>fileList</key>
        # <array>
        #     <filename>./</filename>
        # </array>
        if(struct[index].text == 'fileList'):
            fileList = struct[index+1][0]
            fileList.text = './'		
    # <map type="GFileNameMap">
    #     <key>data</key>
    #     <struct type="DataFile">
    #         <key>name</key>
    #         <filename>G:/proJect/FightDay/Porject/Assets/Tmp/vip/test.txt</filename>
    #     </struct>
    #     <key>datafile</key>
    #     <struct type="DataFile">
    #         <key>name</key>
    #         <filename></filename>
    #     </struct>
    # </map>
    GFileNameMap = root.find(".//*[@type='GFileNameMap']")
    GFileNameMap[1][1].text = tpsheetExportPath
    GFileNameMap[3][1].text = ""
    wiritePath ='./'+ prepath +'/'+export+'.tps'
    tree.write(wiritePath,encoding='UTF-8', xml_declaration=True)
    cmd = "TexturePacker " + wiritePath
    print cmd
    subprocess.call(cmd,shell=True)

	##NOTICE : convert to uft8  
    mconvert_to_utf8(tpsheetExportPath, really_tpsheetExportPath)

def getPngList(curAnimPath):
    pngList = []
    for png in os.listdir(curAnimPath):
        if os.path.splitext(png)[1]=='.png':
            pngList.append(png)
    return pngList

with open("config.json","r") as jsonFile:
    config = json.load(jsonFile)

# 模板路径
templatePath = config['templatePath']
# 输出前置目录
exportPrePath = config['exportPrePath'];

# 原始图片目录列表
preList = config['preList']
# 导出的 图集 和 tpsheet 名字
exportList = config['exportList']

for index in range(len(preList)):
    # .Solider/1/Blue/charge
    prePath = preList[index]
    # charge
    export = exportList[index]
    # print ' prePath ' + prePath + 'export '+export
    # G:/proJect/FightDay/Porject/Assets/Tmp/vip/test.tga
    # G:/proJect/FightDay/Porject/Assets/Tmp/vip/test.txt
    pngExportPath = exportPrePath + '/' + prePath + '/' + export +'.tga'
    tpsheetExportPath = exportPrePath + '/' + prePath + '/' + export +'_back.txt'
    really_tpsheetExportPath = exportPrePath + '/' + prePath + '/' + export +'.txt'
    exportTps(tpsheetExportPath, really_tpsheetExportPath, pngExportPath, prePath, export)


