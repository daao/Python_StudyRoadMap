#! /usr/bin/env python
#coding:utf8

import shutil


project_name = "xxju"; #项目名字

src =  "/Users/zjy/Development/UnityCoreFrameWork/AndroidProject" # 导出的android工程路径
des = "/Users/zjy/Development/Unity-Android/TemplateCodeProject"; # 导出的android工程模板路径


# copy assets
src_path = src + "/" + project_name + "/assets";
print "src_path", src_path;
des_path = des + "/" + project_name + "/assets";
print "des_path", des_path;
shutil.rmtree(des_path); # 递归删除目录下的所有文件
shutil.move(src_path, des_path);
# copy lib
src_path = src + "/" + project_name + "/libs";
des_path = des + "/" + project_name + "/libs";
shutil.rmtree(des_path);
shutil.move(src_path, des_path);