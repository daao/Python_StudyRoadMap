#Flow Controller


## loop

    #! /usr/bin/env python
    #coding:utf8

    fruits = ['banana', 'apple',  'mango']
    for index in range(len(fruits)):
    print 'Current fruit :', fruits[index]

    print "Good bye!"

    Current fruit : banana
    Current fruit : apple
    Current fruit : mango
    Good bye!

## switch 

[Why isn’t there a switch or case statement in Python?](https://docs.python.org/2/faq/design.html#why-isn-t-there-a-switch-or-case-statement-in-python)


    def case1(somearg):
        pass
    def case2(somearg):
        pass
    def case3(somearg):
        pass

    switch={
    1: case1,
    2: case2,
    3: case3
    }

    switch[case](arg)