#! /usr/bin/env python
#coding:utf8

import sys
import subprocess
import shutil

reload(sys)
sys.setdefaultencoding('utf-8')

project_name = "xxjn"; #项目名字
projectPath = "/Users/zjy/Development/Trunck"; # Unity 项目路径s
# projectPath = "/Users/zjy/Development/UnityCoreFrameWork/Project"
src = projectPath+"/AndroidProject" # 导出的android工程路径
# template_path = "/Users/zjy/Development/Porject_Tem_And"; # 导出的android工程模板路径
template_path= "/Users/zjy/Development" 

unity_4_path = "/Applications/Unity/Unity.app/Contents/MacOS/Unity" 
# or C:\program files\Unity\Editor\Unity.exe
# Unity 路径
executeMethod  = "Build.BuildAndroidProject";
# 打包方法
logfile = "./build.log";
needlog = True;

# Step 1 : Build Android Project  
# /Applications/Unity/Unity.app/Contents/MacOS/Unity -batchmode  -projectPath /Users/zjy/Development/UnityCoreFrameWork/Project -executeMethod  Build.BuildAndroidProject -quit

# cmd = unity_4_path + " -batchmode " + " -projectPath " + projectPath + " -executeMethod  " + executeMethod;
# if needlog:
#     cmd += " -logFile " + logfile;
# cmd += " -quit";
# print "cmd",cmd
# subprocess.call(cmd, shell = True)

# Step 2 : copy => asset / lib = > template projectPath

# src_path = src + "/" + project_name + "/assets";
# print "src_path", src_path;
# des_path = template_path + "/" + project_name + "/assets";
# print "des_path", des_path;
# shutil.rmtree(des_path); # 递归删除目录下的所有文件
# shutil.move(src_path, des_path);

# copy lib
# src_path = src + "/" + project_name + "/libs";
# des_path = template_path + "/" + project_name + "/libs";
# shutil.rmtree(des_path);
# shutil.move(src_path, des_path);


# Step 3: ant build

# ant -buildfile  /Users/zjy/Development/Unity-Android/TemplateCodeProject/xxju/build.xml clean
# ant -buildfile  /Users/zjy/Development/Unity-Android/TemplateCodeProject/xxju/build.xml release

ant_build = "ant -buildfile " + template_path + "/" + project_name + "/build.xml " + "clean";
print "ant_build",ant_build
subprocess.call(ant_build, shell = True)
ant_build = "ant -buildfile " + template_path + "/" + project_name + "/build.xml " + "debug";
print "ant_build",ant_build
subprocess.call(ant_build, shell = True)
