from PIL import Image

path = "mask.png"
im = Image.open(path)
print im.format, im.size, im.mode

r, g, b = im.split()

# 2
print "LA", Image.getmodebands("LA")
# 1
print "L", Image.getmodebands("L")

newImage = Image.merge("LA", (r,g))
newImage.save("test.png","PNG")
