# PIL 库练习

## Src

pil-handbook.pdf 手册

splite_alpha.py 分离alpha 通道

## Tutorial


## install 

[window install link](https://pypi.python.org/pypi/Pillow) 

Notes

    Pillow < 2.0.0 supports Python versions 2.4, 2.5, 2.6, 2.7.
    Pillow >= 2.0.0 < 3.5.0 supports Python versions 2.6, 2.7, 3.2, 3.3, 3.4, 3.5
    Pillow >= 3.5.0 supports Python versions 2.7, 3.3, 3.4, 3.5, 3.6

## tutorial

    

## reference

[doc link](https://pillow.readthedocs.io/en/4.0.x/installation.html)

[Python Windows下安装PIL遇到的问题](http://blog.csdn.net/fanoluo/article/details/42528169)

[tutorial](http://effbot.org/imagingbook/)

[pil-handbook](http://www.pythonware.com/media/data/pil-handbook.pdf)