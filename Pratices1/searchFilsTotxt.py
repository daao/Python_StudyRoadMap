#! /usr/bin/env python
#coding:utf8

import sys
import os

#遍历当前目录下的所有文件

reload(sys)
sys.setdefaultencoding('utf-8')


L = []			
max = 1500

def search(path):
    for x in os.listdir(path):
        fp = os.path.join(path, x)
        if os.path.isfile(fp) and os.path.splitext(fp)[1]=='.cs':
            # print fp
            L.append(fp)
        elif os.path.isdir(fp):
            search(fp)

# search('.')


def write():
	i = 0
	with open('test.txt','w+') as my_txt:
		for f in L:
			# print f
			with open(f, 'r') as file:
				for line in file.readlines():
					# print line
					my_txt.write(line)
					i+=1
					# print i
					if i >= max:
						return
						



if __name__ == '__main__':
	search('.')
	write()