using UnityEngine;
using System;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class CfgNewBattleGuideVo
{
    public int index;
    public int triggerCondition;
    public int guideTaskType;
    public string para1;
    public string para2;
    public string para3;

    public CfgNewBattleGuideVo(string[] arr)
    {
         this.index=int.Parse(arr[0]);
         this.triggerCondition=int.Parse(arr[1]);
         this.guideTaskType=int.Parse(arr[2]);
         this.para1=(arr[3]);
         this.para2=(arr[4]);
         this.para3=(arr[5]);
    }

    public override string ToString()
    {
        StringBuilder builder = new StringBuilder();

        FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
        foreach (var item in fields)
        {
            builder.Append(item.Name + " : " + item.GetValue(this));
        }

        return builder.ToString();
    }
}
