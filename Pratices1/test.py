#! /usr/bin/env python
#coding:utf8

import sys
import os

reload(sys)
sys.setdefaultencoding('utf-8')


def search(path):
    for x in os.listdir(path):
        fp = os.path.join(path, x)
        if os.path.isfile(fp) and os.path.splitext(fp)[1]=='.py':
            print fp
        elif os.path.isdir(fp):
            search(fp, s)

search('.')