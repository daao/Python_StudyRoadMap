#! /usr/bin/env python
#coding:utf8

import sys
import subprocess
import shutil

reload(sys)
sys.setdefaultencoding('utf-8')

project_name = "xxju"; #项目名字
projectPath = "/Users/zjy/Development/UnityCoreFrameWork/Project"; # Unity 项目路径s
src = "/Users/zjy/Development/UnityCoreFrameWork/AndroidProject" # 导出的android工程路径
template_path = "/Users/zjy/Development/Unity-Android/TemplateCodeProject"; # 导出的android工程模板路径

unity_4_path = "/Applications/Unity/Unity.app/Contents/MacOS/Unity" 
# or C:\program files\Unity\Editor\Unity.exe
# Unity 路径
executeMethod  = "Build.BuildAndroidProject";
# 打包方法
logfile = "./build.log";
needlog = False;

# Step 1 : Export Android Project
cmd = unity_4_path + " -batchmode " + " -projectPath " + projectPath + " -executeMethod " + executeMethod + " -quit"
print "cmd",cmd
subprocess.call(cmd, shell = True)

# Step 2 : Replace asset/lib
srcpath = src + "/" + project_name + "/assets";
print srcpath
despath = template_path + "/" + project_name + "/assets";
print despath
shutil.rmtree(despath)
shutil.move(srcpath, despath)

srcpath = src + "/" + project_name + "/libs";
print srcpath
despath = template_path + "/" + project_name + "/libs";
print despath
shutil.rmtree(despath)
shutil.move(srcpath, despath)

#Setp 3 : ant release

ant_clean = "ant -buildfile " + template_path + "/" + project_name + "/build.xml" + " clean";
print ant_clean
subprocess.call(ant_clean, shell = True)
ant_release = "ant -buildfile " + template_path + "/" + project_name + "/build.xml" + " release";
print ant_release
subprocess.call(ant_release, shell = True)