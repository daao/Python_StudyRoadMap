#! /usr/bin/env python
#coding:utf8

# TextuePacker 根据目录批量导出 tps 然后根据 tps 批量导出 u3d 图集和tpsheet

# https://docs.python.org/2.7/library/xml.etree.elementtree.html#module-xml.etree.ElementTree

import sys
import os
import xml.etree.ElementTree as ET
import subprocess

reload(sys)
sys.setdefaultencoding('utf-8')



# 输出前置目录
# G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model + /Solider/1/Blue + /charge
exportPrePath ="E:/xxjnAiOpt/Assets/Sprites/UI";

# 图片目录
# ./Solider/1/Blue + /charge
# ./Solider/1/Blue + /dead

# 原始图片目录列表
preList=['Solider/1/Blue/charge','Solider/1/Blue/dead1']

# 导出的 图集 和 tpsheet 名字
exportList=['charge','dead1']

# 模板路径
templatePath = 'template.tps'

def exportTps(tpsheetExportPath, pngExportPath, prepath,export):
    tree = ET.parse(templatePath)
    root = tree.getroot()
    struct = root.find('struct')
    for	index in range(len(struct)):
        # G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/Solider/1/Blue/charge/charge.png
        if(struct[index].text == 'textureFileName'):
            # print index+1,struct[index+1].tag,struct[index+1].text
            struct[index+1].text = pngExportPath
        # <key>textureFileName</key>
        # <filename>G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/test/Sprites.png</filename>
        if(struct[index].text == 'fileList'):
            fileList = struct[index+1][0]
            # ./
            fileList.text = './'
        # <key>fileList</key>
        # <array>
        #     <filename>G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/Solider/1/Blue/charge</filename>
        # </array>

    # G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/Solider/1/Blue/charge/charge.png

    GFileNameMap = root.find(".//*[@type='GFileNameMap']")
    GFileNameMap[3][1].text = tpsheetExportPath
    # <map type="GFileNameMap">
        # <key>data</key>
        # <struct type="DataFile">
        #     <key>name</key>
        #     <filename></filename>
        # </struct>
        # <key>datafile</key>
        # <struct type="DataFile">
        #     <key>name</key>
        #     <filename>G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/test/Sprites.tpsheet</filename>
        # </struct>
    # </map>

    wiritePath ='./'+ prepath +'/'+export+'.tps'
    tree.write(wiritePath,encoding='UTF-8', xml_declaration=True)
    cmd = "TexturePacker " + wiritePath
    print cmd
    subprocess.call(cmd,shell=True)

def getPngList(curAnimPath):
    pngList = []
    for png in os.listdir(curAnimPath):
        if os.path.splitext(png)[1]=='.png':
            pngList.append(png)
    return pngList

for index in range(len(preList)):
    # .Solider/1/Blue/charge
    prePath = preList[index]
    # charge
    export = exportList[index]
    # ['soilder-JMrun_0.png', 'soilder-JMrun_1.png', 'soilder-JMrun_10.png',
    #  'soilder-JMrun_11.png', 'soilder-JMrun_12.png', 'soilder-JMrun_13.png',
    #  'soilder-JMrun_14.png', 'soilder-JMrun_15.png', 'soilder-JMrun_2.png',
    #  'soilder-JMrun_3.png', 'soilder-JMrun_4.png', 'soilder-JMrun_5.png',
    #  'soilder-JMrun_6.png', 'soilder-JMrun_7.png', 'soilder-JMrun_8.png', 'soilder-JMrun_9.png']
    # pngList = getPngList('./'+prePath) 这个可以不用
    # print pngList
    # G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/Solider/1/Blue/charge/charge.png
    # G:/proJect/FightDay/Porject/Assets/Sprites/battle/Model/Solider/1/Blue/charge/charge.tpsheet
    pngExportPath = exportPrePath + '/' + prePath + '/' + export +'.png'
    tpsheetExportPath = exportPrePath + '/' + prePath + '/' + export +'.tpsheet'
    exportTps(tpsheetExportPath,pngExportPath,prePath,export)



